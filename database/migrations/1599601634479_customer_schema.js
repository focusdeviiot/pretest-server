'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.create('customers', (table) => {
      table.increments()
      table.string('uuid', 200).notNullable().unique()
      table.string('first_name', 254).notNullable()
      table.string('last_name', 254).notNullable()
      table.string('nick_name', 254).nullable()
      table.string('email', 254).notNullable().unique()
      table.string('password', 254).notNullable()
      table.string('facebook', 254).nullable()
      table.string('twitter', 254).nullable()
      table.string('line', 254).nullable()
      table.string('youtube', 254).nullable()
      table.string('website', 254).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('customers')
  }
}

module.exports = CustomerSchema
