'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() =>{
  Route.get('show', 'UserController.show');
  Route.post('createuser', 'CustomerController.CreateUser')
  Route.get('getuser', 'CustomerController.GetUser');
  Route.post('edituser', 'CustomerController.EditUser')
  Route.post('deleteuser', 'CustomerController.DeleteUser')
}).prefix('api')
