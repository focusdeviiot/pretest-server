'use strict'
const { v4: uuidv4 } = require('uuid');
var mysql = require('mysql');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with customers
 */


class CustomerController {
  /**
   * Render a form to update an existing customer.
   * GET customers/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async CreateUser ({ request, response }) {
    const {first_name, last_name, nick_name, email, password, facebook, twitter, line, youtube, website} = request.post();
    let res_data = {data : '', msg: '', isOk: false};

    if(typeof first_name !== 'undefined'
        && typeof last_name !== 'undefined'
        && typeof email !== 'undefined'
        && typeof password !== 'undefined'
        && first_name
        && last_name
        && email
        && password
        ){
          const Customer = use('Database').table('customers');
          const uuid = uuidv4();
          try {
            await Customer.insert({uuid, first_name, last_name, nick_name, email, password, facebook, twitter, line, youtube, website});
            res_data.data = null;
            res_data.msg = 'Working Created User';
            res_data.isOk = true;
          } catch (error) {
            console.error(error);
            res_data.data = null;
            res_data.msg = 'Error!! -> ' + error;
            res_data.isOk = false;
          }
    }
    else{
      res_data.data = null;
      res_data.msg = 'Error!! first_name, last_name, email, password is Null';
      res_data.isOk = false;
    }
    return response.json(res_data);
  }

  async GetUser ({ response }) {
    const Customer = use('Database').table('customers');
    let res_data = {data : '', msg: '', isOk: false};
    try {
      const customer = await Customer.select('*');
      res_data.data = customer;
      res_data.msg = 'Working Get User';
      res_data.isOk = true;
    } catch (error) {
      res_data.data = null;
      res_data.msg = 'Error Get User -> ' + error;
      res_data.isOk = false;
    }
    return response.json(res_data);
  }

  async EditUser ({ request, response }) {
    const Customer = use('Database').table('customers');
    const {uuid, first_name, last_name, nick_name, email, password, facebook, twitter, line, youtube, website} = request.post();
    let res_data = {data : '', msg: '', isOk: false};
    try {
      await Customer.where('uuid', uuid)
            .update('first_name', first_name)
            .update('last_name', last_name)
            .update('nick_name', nick_name)
            .update('email', email)
            .update('password', password)
            .update('facebook', facebook)
            .update('twitter', twitter)
            .update('line', line)
            .update('youtube', youtube)
            .update('website', website);
      res_data.data = null;
      res_data.msg = 'Working Edit User';
      res_data.isOk = true;
    } catch (error) {
      res_data.data = null;
      res_data.msg = 'Error Edit User -> ' + error;
      res_data.isOk = false;
    }
    return response.json(res_data);
  }

  async DeleteUser ({ request, response }) {
    const {uuid} = request.post();
    const Customer = use('Database').table('customers');
    let res_data = {data : '', msg: '', isOk: false};
    try {
      Customer.where('uuid', uuid).del();
      res_data.data = null;
      res_data.msg = 'Working Delete User';
      res_data.isOk = true;
    } catch (error) {
      res_data.data = null;
      res_data.msg = 'Error Delete User -> ' + error;
      res_data.isOk = false;

    }
    return response.json(res_data);
  }


}

module.exports = CustomerController
