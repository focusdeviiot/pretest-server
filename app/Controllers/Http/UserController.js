'use strict'

class UserController {
  show ({ response }){
    return response.json('Hello World');
  }

  CreateUser ({ request, response }){
    const {email, password} = request.post()
    let msg = '';

    if (typeof email !== 'undefined' && email){
      if (typeof password !== 'undefined' && password) {
        msg = 'email and password working!!';
      }
      else{
        msg = 'email working!! but password = null';
      }
    }
    else{
      msg = 'email = null!!';
    }
    return response.json(msg);
  }
}


module.exports = UserController
